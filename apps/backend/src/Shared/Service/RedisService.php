<?php

namespace App\Shared\Service;

use Predis\Client;
use Psr\SimpleCache\CacheInterface;

class RedisService implements CacheInterface
{
    private Client $redis;
    private const TIMEOUT_ONE_DAY = 60 * 60 * 24;

    public function __construct()
    {
        $this->redis = new Client([
            'host' => $_ENV['REDIS_HOST'],
            'port'   => $_ENV['REDIS_PORT'],
        ]);
    }

    public function get(string $key, mixed $default = null): mixed
    {
        $value = $this->redis->get($key);

        return $value === false ? $default : $value;
    }

    public function set(string $key, mixed $value, \DateInterval|int|null $ttl = 5): bool
    {
        if ($ttl instanceof \DateInterval) {
            $ttl = (new \DateTime('@0'))->add($ttl)->getTimestamp();
        }

        $this->redis->set($key, $value, 'EX', $ttl);
        return true;
    }

    public function delete(string $key): bool
    {
        return $this->redis->del($key) === 1;
    }

    public function clear(): bool
    {
        return $this->redis->flushdb();
    }

    public function getMultiple(iterable $keys, mixed $default = null): iterable
    {
        $values = $this->redis->mget((array) $keys);

        foreach ($values as $key => $value) {
            $result[$keys[$key]] = $value === false ? $default : $value;
        }

        return $result;
    }

    public function setMultiple(iterable $values, \DateInterval|int|null $ttl = self::TIMEOUT_ONE_DAY): bool
    {
        $values = (array) $values;
        $result = $this->redis->mset($values);

        if ($ttl !== null) {
            foreach (array_keys($values) as $key) {
                if ($ttl instanceof \DateInterval) {
                    $ttl = (new \DateTime('@0'))->add($ttl)->getTimestamp();
                }
                $this->redis->expire($key, (int) $ttl);
            }
        }

        return $result;
    }

    public function deleteMultiple(iterable $keys): bool
    {
        $keys = (array) $keys;
        return $this->redis->del($keys) === count($keys);
    }

    public function has(string $key): bool
    {
        return $this->redis->exists($key);
    }
}