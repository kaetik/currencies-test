<?php

namespace App\Currencies\Domain\Entity;

class Currency
{

    public function __construct(
        public string $alphaCode,
        public string $code,
        public string $date,
        public float $inverseRate,
        public string $name,
        public string $numericCode,
        public float $rate
    )
    {
    }

}