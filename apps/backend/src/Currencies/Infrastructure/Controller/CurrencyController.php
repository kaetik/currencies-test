<?php

namespace App\Currencies\Infrastructure\Controller;

use App\Currencies\Infrastructure\Service\CurrencyService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\MapQueryParameter;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/currencies', name: 'currencies', methods: ['GET'])]
class CurrencyController extends AbstractController
{
    public function __construct(private readonly CurrencyService $currencyService)
    {
    }

    #[Route('/converter')]
    public function getCurrenciesForConver(
        #[MapQueryParameter] string $from,
        #[MapQueryParameter] string $to,
    ): Response {
        $result = $this->currencyService->getCurrenciesConverter($from, $to);
        return new JsonResponse($result);
    }

    #[Route('/list')]
    public function getCurrenciesList(): Response
    {
        $result = $this->currencyService->getCurrenciesList();
        return new JsonResponse($result);
    }

    #[Route('/popular')]
    public function getPopularCurrencies(): Response
    {
        $result = $this->currencyService->getPopularCurrencies();
        return new JsonResponse($result);
    }
}
