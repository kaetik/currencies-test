<?php
namespace App\Currencies\Infrastructure\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use App\Shared\Service\RedisService;
use App\Currencies\Domain\Entity\Currency;

class CurrencyService {
    private const POPULAR_CURRENCIES = ['eur', 'jpy', 'gbp', 'aud', 'uah'];
    private const CACHE_CURRENCIES_TIME = 60 * 5; // 5 min

    /**
     * @var Currency[]
     */
    private array $currencies = [];
    public function __construct( private readonly RedisService $redis, private readonly HttpClientInterface $httpClient)
    {

    }

    /**
     * @param string $from
     * @param string $to
     * @return array currencies data for converting
     */
    public function getCurrenciesConverter(string $from, string $to): array {
        $this->requestCurrenciesData();

        return [
            'from'  => $this->currencies[$from],
            'to'    => $this->currencies[$to]
        ];
    }


    /**
     * @return array the list of all currencies names
     */
    public function getCurrenciesList(): array {
        $this->requestCurrenciesData();
        foreach ($this->currencies as $value) {
            $result[] = ['code' => $value->code, 'name' => $value->name];
        }
        return $result;
    }

    /**
     * @const array POPULAR_CURRENCIES the list of popular currencies
     * @return array the list of popular currencies
     */
    public function getPopularCurrencies(): array {
        $this->requestCurrenciesData();
        $result = [];

        foreach (self::POPULAR_CURRENCIES as $currency) {
            $result[] = [
                'name' => $this->currencies[$currency]->name,
                'rate' => $this->currencies[$currency]->rate
            ];
        }

        return $result;
    }

    /**
     * Getting currencies data
     */
    private function requestCurrenciesData(): void {
        if ( !$this->redis->has('currencies')) {
            $response = $this->httpClient->request('GET', 'https://www.floatrates.com/daily/usd.json');

            $this->redis->set('currencies', $response->getContent(), self::CACHE_CURRENCIES_TIME);
        }
        /**
         * Create first currency "usd"
         */
        $allCurrencies = array_merge(
            ['usd' => new Currency('USD', 'USD', '', 1, 'United State Dollar', '', 1)],
            (array) json_decode($this->redis->get('currencies'))
        );

        $this->currencies = $allCurrencies;
    }
}