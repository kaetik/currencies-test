#!/bin/sh
# Shell strict mode:
# - Abort if process return non-zero exit code
# - Show undefined variable error
# - Abort pipeline if process return non-zero exit code
set -euo pipefail

composer install --ignore-platform-reqs

php-fpm