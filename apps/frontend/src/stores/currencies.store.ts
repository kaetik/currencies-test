import { defineStore, acceptHMRUpdate } from 'pinia'
import { CurrenciesListItems, CurrensiesConverter, PopularCurrencies, QueryObject } from '../interfaces/currencies.interface'
import axios from 'axios';

const transformToGetRequest = function(route: string, obj: QueryObject) {
  const queryPrepare = [];
  for (const p in obj)
    if (Object.prototype.hasOwnProperty.call(obj, p)) {
      queryPrepare.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  const query = queryPrepare.join("&");
  return `${route}?${query}`;
}

export const currenciesStore = defineStore({
  id: 'currencies',
  state: () => ({
    currenciesList: [] as CurrenciesListItems[],
    currenciesConverter: {} as CurrensiesConverter,
    popularCurrencies: [] as PopularCurrencies[]
  }),
  getters: {
  },
  actions: {
    async getCurrenciesList() {
      try {
        const currenciesList =  await axios.get('/api/currencies/list');

        this.currenciesList = currenciesList.data;
      } catch (err: any) {
        console.error(`Erorr: ${err.message}`);
      }
    },

    async getCurrenciesConverter(query: QueryObject) {
      try {
        const currenciesConverter =  await axios.get(transformToGetRequest('/api/currencies/converter', query));

        this.currenciesConverter = currenciesConverter.data;
      } catch (err: any) {
        console.error(`Erorr: ${err.message}`);
      }
    },

    async getPopularCurrencies() {
      try {
        const popularCurrencies =  await axios.get('/api/currencies/popular');

        this.popularCurrencies = popularCurrencies.data;
      } catch (err: any) {
        console.error(`Erorr: ${err.message}`);
      }
    },
  },
})

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(currenciesStore, import.meta.hot))
}
