/**
 * plugins/index.ts
 *
 * Automatically included in `./src/main.ts`
 */

// Plugins
import vuetify from './vuetify'
import router from '../router'
import { pinia } from './pinia'
import axios from 'axios'
import VueAxios from 'vue-axios'
// Types
import type { App } from 'vue'

export function registerPlugins (app: App) {
  app
    .use(vuetify)
    .use(pinia)
    app.use(VueAxios, axios)
    .use(router)
}
