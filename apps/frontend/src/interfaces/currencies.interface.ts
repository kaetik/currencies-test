export interface CurrenciesListItems {
  code: string;
  name: string;
}

export interface Currency {
  alphaCode: string,
  code: string,
  date: string,
  inverseRate: number,
  name: string,
  numericCode: string,
  rate: number
}

export interface QueryObject {
  [key: string]: string;
}

export interface CurrenciesConvertedPayload {
  from: string;
  to: string
}

export interface PopularCurrencies {
  name: string;
  rate: number
}

export interface CurrensiesConverter {
  from: Currency,
  to: Currency

}
